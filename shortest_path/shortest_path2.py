"""
В таблице n ×  n некоторые клетки заняты шариками, другие свободны. Выбран
шарик, который нужно переместить, и место, куда его нужно переместить.
Выбранный шарик за один шаг перемещается в соседнюю по горизонтали или
вертикали свободную клетку.

Напишите программу, которая определяет, возможно ли переместить шарик из
изначальной клетки в заданную, и если возможно, то находит путь из наименьшего
количества шагов.

Формат входных данных
На вход программе в первой строке подается число n, где 2 ≤ n ≤ 40, далее
следуют n строк длины n, представляющие собой таблицу. Точкой . обозначается
свободная клетка, латинской заглавной O— шарик, собачкой @— исходное положение
шарика, который должен двигаться, латинской заглавной X — конечное положение
шарика.

Формат выходных данных
Программа должна вывести YES, если движение возможно, или NO, если нет. Если
движение возможно, следует также вывести исходную таблицу n×n, обозначив на
ней путь плюсами +.

Примечание 1. Если движение шарика возможно, конечная точка Х также должна
быть обозначена плюсом +.

Примечание 2. Если вариантов добраться до конечной точки несколько, можно
вывести любой.
"""


from collections import defaultdict
from math import inf
from time import perf_counter
import heapq as heap


# Reading input
n = int(input())
space = [[symbol for symbol in input()] for _ in range(n)]
t1 = perf_counter()

# Constructing graph
graph = defaultdict(dict)
start = None
end = None
for i in range(n):
    for j in range(n):
        if space[i][j] == "O":
            continue
        if space[i][j] == "X":
            start = (i, j)
        if space[i][j] == "@":
            end = (i, j)
        if i != 0 and space[i - 1][j] != "O":
            graph[f"{i}-{j}"][f"{i - 1}-{j}"] = 1
        if i != n - 1 and space[i + 1][j] != "O":
            graph[f"{i}-{j}"][f"{i + 1}-{j}"] = 1
        if j != 0 and space[i][j - 1] != "O":
            graph[f"{i}-{j}"][f"{i}-{j - 1}"] = 1
        if j != n - 1 and space[i][j + 1] != "O":
            graph[f"{i}-{j}"][f"{i}-{j + 1}"] = 1
t2 = perf_counter()


# Dijkstra algorythm
def dijkstra(
        G: "dict[dict[str, int]]",
        startingNode: str
        ) -> "tuple[dict[str, str], dict[str, int]]":
    visited = set()
    parentsMap = {}
    pq = []
    nodeCosts = defaultdict(lambda: inf)
    nodeCosts[startingNode] = 0
    heap.heappush(pq, (0, startingNode))
    while pq:
        # go greedily by always extending the shorter cost nodes first
        _, node = heap.heappop(pq)
        visited.add(node)
        for adjNode, weight in G[node].items():
            if adjNode in visited:
                continue
            newCost = nodeCosts[node] + weight
            if nodeCosts[adjNode] > newCost:
                parentsMap[adjNode] = node
                nodeCosts[adjNode] = newCost
                heap.heappush(pq, (newCost, adjNode))
    return parentsMap, nodeCosts


parents, _ = dijkstra(graph, f"{start[0]}-{start[1]}")
t3 = perf_counter()

# Print result
if f"{end[0]}-{end[1]}" not in parents:
    print("NO")
    exit()
print("YES")
parent = parents[f"{end[0]}-{end[1]}"]
while parent != f"{start[0]}-{start[1]}":
    i, j = [int(_) for _ in parent.split("-")]
    space[i][j] = "+"
    parent = parents[parent]
space[start[0]][start[1]] = "+"
for line in space:
    print("".join(line))
t4 = perf_counter()

print(t2 - t1)
print(t3 - t2)
print(t4 - t3)
