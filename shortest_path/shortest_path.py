'''Багдадский вор перемещается по городу размера N×N, перебегая по соседним
справа или снизу клеткам. Добраться до каждой из клеток вор может за какое-то
количество секунд (от 1 до 9), а также на клетке может стоять стражник,
в таком случае клетка будет обозначаться числом 000, и вор не сможет
пройти через нее. Ему нужно добраться из самой левой верхней точки (1;1) в
самую нижнюю правую точку (N;N). Напишите программу, которая рисует маршрут,
по которому вор сумеет пройти весь путь за кратчайшее время. Если такого пути
нет, требуется вывести слово Impossible.

Формат входных данных
В первой строке на вход программе подается натуральное число N, в последующих N
строках подаются строки длины NNN, состоящие из чисел от 0 до 9 включительно.

Формат выходных данных
Программа должна нарисовать маршрут кратчайшего пути из N строк длины N, где
символ # будет означать клетки, по которым вор должен пройти, а символ - –
остальные. Если такого пути нет, следует вывести слово Impossible.

Примечание 1. Гарантируется, что кратчайший путь единственный, если таковой
имеется.

Примечание 2. Обратите внимание на то, что стражник может стоять как в клетке
(1;1), так и в клетке (N;N). В этом случае следует вывести слово Impossible.

Примечание 3. Посмотреть все тесты к задаче можно по ссылке.'''

from math import inf
from time import perf_counter


# Reading input
n = int(input())
city = [[int(symbol) for symbol in input()] for _ in range(n)]

# Filter obvious solutions
if city[0][0] == 0 or city[n - 1][n - 1] == 0:
    print('Impossible')
    exit()
t1 = perf_counter()

# Constructing costs matrix
costs = [[inf] * n for _ in range(n)]
costs[0][0] = city[0][0]
right_max = n
left_min = 1
for j in range(1, n):
    if city[0][j] == 0:
        right_max = j
        break
    costs[0][j] = city[0][j] + costs[0][j - 1]
for i in range(1, n):
    if city[i][0] == 0:
        break
    costs[i][0] = city[i][0] + costs[i - 1][0]
for i in range(1, n):
    for j in range(left_min, n):
        if city[i][j] == 0:
            if j >= right_max:
                right_max = j
                break
            if j == left_min and costs[i][j - 1] == inf:
                left_min += 1
            continue
        if j == n - 1:
            right_max = n
        costs[i][j] = city[i][j] + min(costs[i - 1][j], costs[i][j - 1])
t2 = perf_counter()

# Print result
if costs[n - 1][n - 1] == inf:
    print('Impossible')
    exit()
display = [['-' for _ in range(n)] for _ in range(n)]
display[0][0] = "#"
i = n - 1
j = n - 1
while i + j != 0:
    display[i][j] = "#"
    if i == 0 or costs[i - 1][j] > costs[i][j - 1]:
        j -= 1
    else:
        i -= 1

for line in display:
    print("".join(line))
t3 = perf_counter()

print(t2 - t1)
print(t3 - t2)
