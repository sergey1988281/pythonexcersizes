def get_base_offset(char):
    """Returns base and UTF-8 offset for Cesar cipher taking language and case
       into consideration. May be extended for other languages"""
    ru_upper_start = ord("А")
    ru_upper_finish = ord("Я")
    ru_lower_start = ord("а")
    ru_lower_finish = ord("я")
    ru_base = ru_upper_finish - ru_upper_start + 1
    en_upper_start = ord("A")
    en_upper_finish = ord("Z")
    en_lower_start = ord("a")
    en_lower_finish = ord("z")
    en_base = en_upper_finish - en_upper_start + 1
    char_code = ord(char)
    # Upper case
    if char.isupper():
        # RU language
        if ru_upper_start <= char_code <= ru_upper_finish:
            return ru_base, ru_upper_start
        # EN language
        else:
            return en_base, en_upper_start
    # Lower case
    else:
        # RU language
        if ru_lower_start <= char_code <= ru_lower_finish:
            return ru_base, ru_lower_start
        # EN language
        else:
            return en_base, en_lower_start


def cipher(char, key, base, offset, action="E"):
    """Performs shift of characted by key value forward or backward
       e - forward, b - backward"""
    char_code = ord(char)
    if action == "E":
        return chr(offset + (char_code - offset + key) % base)
    elif action == "D":
        return chr(offset + (char_code - offset - key) % base)


def cesar_encrypt(text: str, key: int, action="E") -> str:
    """Implements cesar cypher algorythm over text"""
    result = []
    for symbol in text:
        if not symbol.isalpha():
            result.append(symbol)
            continue
        result.append(cipher(symbol,
                             key,
                             *get_base_offset(symbol),
                             action=action))
    return "".join(result)


if __name__ == "__main__":
    """
    Direction: Encrypt/Decrypt
    Encryption key: Integer number
    Encryption text
    """
    while True:
        do_continue = input(("Would you like to encrypt or"
                             "decrypt somethig?:(Y/n)")).upper()
        if do_continue in ("N", "NO"):
            print("Good bye!")
            exit()
        if do_continue not in ("Y", "YES"):
            print("Invalid option, please retry")
            continue
        action = input("What you want?: (E/d)").upper()
        if action not in ("E", "D"):
            print("Invalid option, please retry")
            continue
        key = input("Provide key: (Integer))")
        if not key.isdigit():
            print("Invalid option, please retry")
            continue
        text = input("Type message to process:")
        print(
            "Here is your result:",
            cesar_encrypt(text, int(key), action=action)
        )
