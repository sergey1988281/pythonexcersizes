def print_matrix(matrix: list, n: int, m: int, width=1) -> None:
    """Prints matrix in a pretty format"""
    for r in range(n):
        for c in range(m):
            print(str(matrix[r][c]).ljust(width), end=' ')
        print()


def diagonal_matrix(n: int, m: int) -> list:
    """Generates matrix with diagonal chain"""
    matrix = [[0] * m for _ in range(n)]
    i = j = 0
    current = 1
    while True:
        matrix[i][j] = current
        current += 1
        if i + 1 != n and j - 1 != -1:
            i += 1
            j -= 1
            continue
        else:
            for row in range(len(matrix)):
                match = matrix[row].index(0) if 0 in matrix[row] else None
                if match is not None:
                    i = row
                    j = match
                    break
            else:
                break
    return matrix


def spiral_matrix(n: int, m: int) -> list:
    """Generates matrix with spiral chain"""
    matrix = [[0] * m for _ in range(n)]
    direction = "right"
    i = j = 0
    current_value = 1
    while True:
        matrix[i][j] = current_value
        current_value += 1
        if direction == "right":
            if (j + 1) < m and matrix[i][j + 1] == 0:
                j += 1
                continue
            elif (((j + 1) == m or matrix[i][j + 1] != 0) and
                  ((i + 1) < n and matrix[i + 1][j] == 0)):
                direction = "down"
                i += 1
                continue
            else:
                break
        elif direction == "down":
            if (i + 1) < n and matrix[i + 1][j] == 0:
                i += 1
                continue
            elif (((i + 1) == n or matrix[i + 1][j] != 0) and
                  ((j - 1) > -1 and matrix[i][j - 1] == 0)):
                direction = "left"
                j -= 1
                continue
            else:
                break
        elif direction == "left":
            if (j - 1) > -1 and matrix[i][j - 1] == 0:
                j -= 1
                continue
            elif (((j - 1) == -1 or matrix[i][j - 1] != 0) and
                  ((i - 1) > -1 and matrix[i - 1][j] == 0)):
                direction = "up"
                i -= 1
                continue
            else:
                break
        elif direction == "up":
            if (i - 1) > -1 and matrix[i - 1][j] == 0:
                i -= 1
                continue
            elif (((i - 1) == -1 or matrix[i + 1][j] != 0) and
                  ((j + 1) < m and matrix[i][j + 1] == 0)):
                direction = "right"
                j += 1
                continue
            else:
                break
    return matrix


if __name__ == "__main__":
    n, m = [int(number) for number in input().split()]
    print_matrix(diagonal_matrix(n, m), n, m, width=3)
    print_matrix(spiral_matrix(n, m), n, m, width=3)
