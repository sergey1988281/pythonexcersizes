import unittest


def any_base_to_ten(number: str, base: int) -> int:
    """Given number in any system translate to ten system"""
    if base > 36:
        raise ValueError("Bases greater 36 are not supported")
    if base <= 9:
        match = [str(i) for i in range(base)]
    else:
        match = ([str(i) for i in range(10)] +
                 [chr(i) for i in range(ord("A"), ord("A") + base - 10)])
    result = 0
    number = number[::-1]
    for i in range(len(number)):
        ten_digit = match.index(number[i])
        if ten_digit != 0:
            result += base ** i * ten_digit
    return result


def ten_to_any_base(number: int, base: int) -> str:
    """Given number in ten systen convert to requested one"""
    if base > 36:
        raise ValueError("Bases greater 36 are not supported")
    if base <= 9:
        match = [str(i) for i in range(base)]
    else:
        match = ([str(i) for i in range(10)] +
                 [chr(i) for i in range(ord("A"), ord("A") + base - 10)])
    result = []
    while number != 0:
        digit = number % base
        result.append(match[digit])
        number //= base
    return("".join(result[::-1]))


class TestBaseSystemConversions(unittest.TestCase):
    def test_binary(self):
        n = 63
        system = 2
        self.assertEqual(n,
                         any_base_to_ten(ten_to_any_base(n, system), system))

    def test_octal(self):
        n = 123
        system = 8
        self.assertEqual(n,
                         any_base_to_ten(ten_to_any_base(n, system), system))

    def test_hex(self):
        n = 6898
        system = 16
        self.assertEqual(n,
                         any_base_to_ten(ten_to_any_base(n, system), system))


if __name__ == "__main__":
    unittest.main()
