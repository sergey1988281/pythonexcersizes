import unittest


def flatNestedListRecusrive(arr):
    """Returns a flat list of expanded any nested ones from input using
       recursion"""
    result = []
    for element in arr:
        if isinstance(element, list):
            result.extend(flatNestedListRecusrive(element))
        else:
            result.append(element)
    return result


def flatNestedListStack(arr):
    """Returns a flat list of expanded any nested ones from input using stack
       data structure"""
    result = []
    stack = list(arr[::-1])
    while stack:
        element = stack.pop()
        if isinstance(element, list):
            stack.extend(element[::-1])
        else:
            result.append(element)
    return(result)


class TestexpandNestedList(unittest.TestCase):
    def test_none(self):
        self.assertEqual(flatNestedListRecusrive([]), [])
        self.assertEqual(flatNestedListStack([]), [])

    def test_general(self):
        self.assertEqual(
            flatNestedListRecusrive([1, [[2, [3, 3.5]], [4, 5]], 6]),
            [1, 2, 3, 3.5, 4, 5, 6])
        self.assertEqual(
            flatNestedListStack([1, [[2, 3], [4, 5]], 6]),
            [1, 2, 3, 4, 5, 6])


if __name__ == '__main__':
    unittest.main()
