from random import randint
from time import perf_counter
from functools import wraps


def matrixCheck(a: list, b: list) -> None:
    """Basic check if matrixes can be multiplied: num columns of first matrix
       equals num rows of second more sophicticated check may be added later
       like all rows has identical length"""
    if len(a[0]) != len(b):
        raise ValueError("Matrix size missmatch")


def generateMatrix(i, j, *args, value_function=randint):
    """Generates matrix of specified size and value using provided function as
       a strategy"""
    return [[value_function(*args)]*i for _ in range(j)]


def timeit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        """Simple decoraor which measures time need for function to complete"""
        start = perf_counter()
        result = func(*args, **kwargs)
        finish = perf_counter()
        return (finish - start), result, func.__name__
    return wrapper
