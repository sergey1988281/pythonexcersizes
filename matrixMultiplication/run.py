from collections import namedtuple
from utils import generateMatrix
from simpleMultiplication import simpleMatrixMultiplication
from threadMultiplication import threadMatrixMultiplication
from threadPoolMultiplication import threadPoolMatrixMultiplication
from processPoolMultiplication import processPoolMatrixMultiplication


if __name__ == "__main__":
    TimedValue = namedtuple("TimedValue", ["time", "result", "func_name"])
    a = generateMatrix(10, 20, 1, 10)
    b = generateMatrix(20, 10, 1, 10)
    simple_result = TimedValue(*simpleMatrixMultiplication(a, b,
                               "threads.log"))
    thread_result = TimedValue(*threadMatrixMultiplication(a, b,
                               "threads.log"))
    thread_pool_result = TimedValue(*threadPoolMatrixMultiplication(a, b,
                                    "threads.log"))
    process_pool_result = TimedValue(*processPoolMatrixMultiplication(a, b,
                                     "threads.log"))
    print(f"{simple_result.func_name}: {simple_result.time * 1000:0.3f}ms:")
    print(f"{thread_result.func_name}: {thread_result.time * 1000:0.3f}ms:")
    print((f"{thread_pool_result.func_name}: "
           f"{thread_pool_result.time * 1000:0.3f}ms:"))
    print((f"{process_pool_result.func_name}: "
           f"{process_pool_result.time * 1000:0.3f}ms:"))
