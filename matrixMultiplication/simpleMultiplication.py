from utils import timeit, matrixCheck


@timeit
def simpleMatrixMultiplication(a: list, b: list, log_file: str) -> list:
    """Fucntion will multiply 2 matrixes in one thread using nested loop,
       each iteration will write to log_file"""
    matrixCheck(a, b)
    result = []
    log_file_full_path = __file__.replace(__file__.split("\\")[-1], log_file)
    with open(log_file_full_path, "a") as file:
        for i in range(len(a)):
            result.append([])
            for j in range(len(b[0])):
                row = a[i]
                column = [k[j] for k in b]
                value = sum([z[0]*z[1] for z in zip(row, column)])
                result[i].append(value)
                file.write(f"Simple {i} {j}:{value}\n")
    return result
