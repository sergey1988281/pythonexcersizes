from threading import Thread, Lock, enumerate, current_thread
from utils import matrixCheck, timeit


def rowColumnMultiplicator(
        result: list,
        i: int,
        j: int,
        row: list,
        column: list,
        log_file: str,
        mutex: Lock
        ) -> None:
    """Function computes scalar product of 2 vectors and inplace update in
       result matrix, result will be written to log_file as well"""
    if(len(row) != len(column)):
        raise ValueError(f"Thread {i} {j} :Row length not equals")
    value = sum([a[0]*a[1] for a in zip(row, column)])
    result[i][j] = value
    with mutex:
        with open(log_file, "a") as file:
            file.write(f"Thread {i} {j}:{value}\n")


@timeit
def threadMatrixMultiplication(a: list, b: list, log_file: str) -> list:
    """Fucntion will multiply 2 matrixes using threads and return result
       matrix, each thread will write logs to a log_file concurrently"""
    matrixCheck(a, b)
    result = []
    log_file_full_path = __file__.replace(__file__.split("\\")[-1], log_file)
    mutex = Lock()
    for i in range(len(a)):
        result.append([])
        for j in range(len(b[0])):
            result[i].append(0)
            row = a[i]
            column = [k[j] for k in b]
            t = Thread(
                    target=rowColumnMultiplicator,
                    args=(
                        result, i, j, row, column, log_file_full_path, mutex,
                        ))
            t.start()
    main_thread = current_thread()
    for thread in enumerate():
        if thread is not main_thread:
            thread.join()
    return result
