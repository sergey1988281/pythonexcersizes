from collections import deque
from threading import Lock
from utils import timeit, matrixCheck
from concurrent.futures import ThreadPoolExecutor


def rowColumnMultiplicator(
        i: int,
        j: int,
        row: list,
        column: list,
        log_file: str,
        mutex: Lock
        ) -> tuple[int, int, int]:
    """Function computes scalar product of 2 vectors and returns result
    together with indexes, result will be written to log_file as well"""
    if(len(row) != len(column)):
        raise ValueError(f"Thread {i} {j} :Row length not equals")
    value = sum([a[0]*a[1] for a in zip(row, column)])
    with mutex:
        with open(log_file, "a") as file:
            file.write(f"ThreadPool {i} {j}:{value}\n")
    return (i, j, value)


@timeit
def threadPoolMatrixMultiplication(a: list, b: list, log_file: str) -> list:
    """Function will multiply 2 matrixes using ThreadPoolExecutor and return
       result matrix, each thread will write logs to a log_file concurrently"""
    matrixCheck(a, b)
    result = []
    log_file_full_path = __file__.replace(__file__.split("\\")[-1], log_file)
    mutex = Lock()
    futures = deque()
    with ThreadPoolExecutor(max_workers=4) as executor:
        for i in range(len(a)):
            result.append([])
            for j in range(len(b[0])):
                result[i].append(0)
                row = a[i]
                column = [k[j] for k in b]
                futures.append(
                    executor.submit(
                        rowColumnMultiplicator,
                        i, j, row, column, log_file_full_path, mutex)
                )
        while futures:
            future = futures.popleft()
            if future.exception():
                raise
            elif future.done():
                i, j, value = future.result()
                result[i][j] = value
            else:
                futures.append(future)
        executor.shutdown()
    return result
