import unittest


class offsetList(list):
    """Class lets to specify from which number to start list index via
       overriding magic methods"""
    def __init__(self, offset=None):
        if(offset is not None):
            self.offset = offset
        else:
            self.offset = 0

    def __getitem__(self, n):
        return super(offsetList, self).__getitem__(n - self.offset)


class Test2Max(unittest.TestCase):
    def test_1(self):
        offset_list = offsetList(1)
        offset_list.append(1)
        offset_list.append(2)
        offset_list.append(3)
        self.assertEqual(offset_list[1], 1)
        self.assertEqual(offset_list[2], 2)
        self.assertEqual(offset_list[3], 3)

    def test_none(self):
        offset_list = offsetList()
        offset_list.append(1)
        offset_list.append(2)
        offset_list.append(3)
        self.assertEqual(offset_list[0], 1)
        self.assertEqual(offset_list[1], 2)
        self.assertEqual(offset_list[2], 3)


if __name__ == '__main__':
    unittest.main()
