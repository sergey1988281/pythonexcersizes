import unittest


def recursiveDeepCopy(arr: list) -> list:
    "Recursively copies list with multiple nested ones"
    result = []
    for i in range(len(arr)):
        if isinstance(arr[i], list):
            result.append(recursiveDeepCopy(arr[i]))
        else:
            result.append(arr[i])
    return result


class TestDeepCopy(unittest.TestCase):
    def test_recursiveDeepCopy(self):
        input = [1, [2, [3, 4]], 5]
        output = recursiveDeepCopy(input)
        self.assertEqual(input, output)
        self.assertIsNot(input, output)
        self.assertEqual(input[1], output[1])
        self.assertIsNot(input[1], output[1])
        self.assertEqual(input[1][1], output[1][1])
        self.assertIsNot(input[1][1], output[1][1])


if __name__ == "__main__":
    unittest.main()
