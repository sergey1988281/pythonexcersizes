import unittest


def secondMax(arr):
    """Returns second maximum in an array of comparable items"""
    if(len(arr) < 2):
        return None
    max1, max2 = None, None
    if(arr[0] == arr[1]):
        max1 = arr[0]
    elif(arr[0] > arr[1]):
        max1, max2 = arr[0], arr[1]
    else:
        max2, max1 = arr[0], arr[1]
    for a in arr[2:]:
        if a > max1:
            max1, max2 = a, max1
        elif (max2 is not None) and (a > max2):
            max2 = a
    return max2


class Test2Max(unittest.TestCase):
    def test_none(self):
        self.assertEqual(secondMax([]), None)
        self.assertEqual(secondMax([1]), None)
        self.assertEqual(secondMax([100, 100]), None)
        self.assertEqual(secondMax([100, 100, 100]), None)

    def test_negative(self):
        self.assertEqual(secondMax([-10, -4, -15, -18]), -10)

    def test_general(self):
        self.assertEqual(secondMax([-10, 0, -15, 18]), 0)


if __name__ == '__main__':
    unittest.main()
