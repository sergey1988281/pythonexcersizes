import unittest
from typing import List


def recursive_binary_search(numbers: List[int], element: int) -> int:
    """Finds index of element in sorted list using recirsive binary search
    algorithm
    Args:
        numbers (List[int]): Sorted list
        element (int): value to find index of
    Returns:
        int: index
    Raises:
        ValueError: In case element with such value doesnt exist in list
    """
    if not numbers:
        raise ValueError()
    if len(numbers) == 1:
        if numbers[0] == element:
            return 0
        raise ValueError()
    middle = len(numbers) // 2 - 1
    if numbers[middle] == element:
        return middle
    elif numbers[middle] > element:
        return recursive_binary_search(numbers[:middle], element)
    else:
        return middle + recursive_binary_search(numbers[middle + 1:],
                                                element) + 1


def binary_search(numbers: List[int], element: int) -> int:
    """Finds index of element in sorted list using binary search algorithm
    in loop
    Args:
        numbers (List[int]): Sorted list
        element (int): value to find index of
    Returns:
        int: index
    Raises:
        ValueError: In case element with such value doesnt exist in list
    """
    start = 0
    end = len(numbers) - 1
    while start <= end:
        middle = (start + end) // 2
        if numbers[middle] == element:
            return middle
        elif numbers[middle] > element:
            end = middle - 1
        else:
            start = middle + 1
    raise ValueError()


class test_(unittest.TestCase):
    def test_recursive_binary_search(self):
        self.assertEqual(
            recursive_binary_search([1, 2, 3, 4, 5], 1),
            0)
        self.assertEqual(
            recursive_binary_search([1, 2, 3, 4, 5], 2),
            1)
        self.assertEqual(
            recursive_binary_search([1, 2, 3, 4, 5], 3),
            2)
        self.assertEqual(
            recursive_binary_search([1, 2, 3, 4, 5], 4),
            3)
        self.assertEqual(
            recursive_binary_search([1, 2, 3, 4, 5], 5),
            4)
        with self.assertRaises(ValueError):
            recursive_binary_search([1, 2, 3, 4, 5], 0)

    def test_binary_search(self):
        self.assertEqual(
            binary_search([1, 2, 3, 4, 5], 1),
            0)
        self.assertEqual(
            binary_search([1, 2, 3, 4, 5], 2),
            1)
        self.assertEqual(
            binary_search([1, 2, 3, 4, 5], 3),
            2)
        self.assertEqual(
            binary_search([1, 2, 3, 4, 5], 4),
            3)
        self.assertEqual(
            binary_search([1, 2, 3, 4, 5], 5),
            4)
        with self.assertRaises(ValueError):
            binary_search([1, 2, 3, 4, 5], 0)


if __name__ == "__main__":
    unittest.main()
