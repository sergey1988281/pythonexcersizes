from collections import deque
from threading import Thread, Condition, current_thread, enumerate
from time import sleep
from datetime import datetime


work = deque()
condition = Condition()
isOkToRun = True


def producer(name: str, delay: int) -> None:
    """Simple producer which genereates some workload to a queue"""
    print(f"Producer {name} initialized with a delay {delay}")
    while isOkToRun:
        sleep(delay)
        with condition:
            task = f"Task produced by {name}"
            work.append(task)
            print(task)
            condition.notify()


def consumer(name: str):
    """Simple consumer which extracts some workload from a queue"""
    print(f"Consumer {name} initialized")
    while isOkToRun:
        with condition:
            while not work:
                condition.wait()
            task = work.popleft()
            print(f"Consumer {name} processes: {task}")


if __name__ == "__main__":
    while True:
        answer = input("What to spawn(c-Consumer, p-Producer, k-Kill All)?:")
        if answer == "c":
            consumer_name = input("Enter name for this consumer:")
            if not consumer_name:
                consumer_name = f"Consumer_{datetime.utcnow()}"
            t = Thread(target=consumer, args=(consumer_name, ))
            t.name = consumer_name
            t.start()
            continue
        if answer == "p":
            producer_name = input("Enter name for this producer:")
            if not producer_name:
                producer_name = f"Producer_{datetime.utcnow()}"
            producer_delay = input(f"Enter delay for this prooducer:")
            if not producer_delay:
                producer_delay = 1
            else:
                try:
                    producer_delay = int(producer_delay)
                except ValueError:
                    print("Invalid delay speciffied")
                    continue
            t = Thread(target=producer, args=(producer_name, producer_delay, ))
            t.name = producer_name
            t.start()
            continue
        if answer == "k":
            isOkToRun = False
            main_thread = current_thread()
            for t in enumerate():
                if t is not main_thread:
                    t.join()
            print(f"Work left: {work}")
            print("Good bye!!!")
            break
        else:
            print("Invalid option!")
