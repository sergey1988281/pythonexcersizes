import unittest


def number_to_words(num):
    """Function spells number in russian"""
    list_1 = ["ноль", "один", "два",  "три", "четыре", "пять", "шесть",
              "семь", "восемь", "девять"]
    list_1_1 = ["десять", "одиннадцать", "двенадцать", "тринадцать",
                "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать",
                "восемнадцать", "девятнадцать"]
    list_x = ["-", "-", "двадцать", "тридцать", "сорок", "пятьдесят",
              "шестьдесят", "семьдесят", "восемьдесят", "девяносто"]
    if num < 10:
        return list_1[num]
    elif num < 20:
        return list_1_1[num % 10]
    elif num % 10 == 0:
        return list_x[num // 10]
    else:
        return list_x[num // 10] + " " + list_1[num % 10]


class TestNumbersToWords(unittest.TestCase):
    def test_1_9(self):
        test_input = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        test_output = ["ноль", "один", "два",  "три", "четыре", "пять",
                       "шесть", "семь", "восемь", "девять"]
        for i in range(len(test_input)):
            self.assertEqual(number_to_words(test_input[i]), test_output[i])

    def test_10_19(self):
        test_input = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
        test_output = ["десять", "одиннадцать", "двенадцать", "тринадцать",
                       "четырнадцать", "пятнадцать", "шестнадцать",
                       "семнадцать", "восемнадцать", "девятнадцать"]
        for i in range(len(test_input)):
            self.assertEqual(number_to_words(test_input[i]), test_output[i])

    def test_20_90(self):
        test_input = [20, 30, 40, 50, 60, 70, 80, 90]
        test_output = ["двадцать", "тридцать", "сорок", "пятьдесят",
                       "шестьдесят", "семьдесят", "восемьдесят", "девяносто"]
        for i in range(len(test_input)):
            self.assertEqual(number_to_words(test_input[i]), test_output[i])

    def test_arbitrary(self):
        test_input = [26, 32, 48, 55, 61, 79, 83, 97]
        test_output = ["двадцать шесть", "тридцать два", "сорок восемь",
                       "пятьдесят пять", "шестьдесят один", "семьдесят девять",
                       "восемьдесят три", "девяносто семь"]
        for i in range(len(test_input)):
            self.assertEqual(number_to_words(test_input[i]), test_output[i])


if __name__ == '__main__':
    unittest.main()
