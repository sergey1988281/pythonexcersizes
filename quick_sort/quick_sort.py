import unittest
from typing import List
from random import randrange
from copy import copy


def recursive_quick_sorted(numbers: List[int]) -> List[int]:
    """Returns new list sorted with quick sort
    Args:
        numbers (List[int]): List of numbers
    Returns:
        List[int]: Sorted list
    """
    size = len(numbers)
    if size < 2:
        return copy(numbers)
    pivot_index = randrange(len(numbers))
    pivot_element = numbers[pivot_index]
    left = [numbers[i] for i in range(size)
            if i != pivot_index and numbers[i] <= pivot_element]
    right = [numbers[i] for i in range(size)
             if i != pivot_index and numbers[i] > pivot_element]
    return (recursive_quick_sorted(left) +
            [pivot_element] +
            recursive_quick_sorted(right))


def inplace_recursive_quick_sorted(numbers: List[int]) -> None:
    """Sort array inplace using quick sort
    Args:
        numbers (List[int]): List of numbers
    """
    def partition(numbers: List[int], start: int, end: int) -> None:
        """Radomly selects an element in array part between start and end
        then moves all elements less than it to left, greater to the right
        Args:
            numbers (List[int]): List
            start (int): Start index
            end (int): End index
        """
        if end - start < 1:
            return
        pivot_idx = randrange(start, end + 1)
        pivot_element = numbers[pivot_idx]
        numbers[start], numbers[pivot_idx] = numbers[pivot_idx], numbers[start]
        low, high = start + 1, end
        while True:
            while low <= high and numbers[high] >= pivot_element:
                high = high - 1
            while low <= high and numbers[low] <= pivot_element:
                low = low + 1
            if low <= high:
                numbers[low], numbers[high] = numbers[high], numbers[low]
            else:
                break
        numbers[start], numbers[high] = numbers[high], numbers[start]
        partition(numbers, start, high - 1)
        partition(numbers, high + 1, end)

    partition(numbers, 0, len(numbers) - 1)


class TestQuickSort(unittest.TestCase):
    def test_recursive_quick_sorted(self):
        self.assertEqual(
            recursive_quick_sorted([4, 5, 3, 1, 2]),
            [1, 2, 3, 4, 5])
        self.assertEqual(
            recursive_quick_sorted([5, 4, 3, 2, 1]),
            [1, 2, 3, 4, 5])

    def test_inplace_recursive_quick_sorted(self):
        array = [4, 5, 3, 1, 2]
        inplace_recursive_quick_sorted(array)
        self.assertEqual(array, [1, 2, 3, 4, 5])
        array = [5, 4, 3, 2, 1]
        inplace_recursive_quick_sorted(array)
        self.assertEqual(array, [1, 2, 3, 4, 5])


if __name__ == "__main__":
    unittest.main()
