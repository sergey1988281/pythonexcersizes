import unittest
from collections import namedtuple


Item = namedtuple('Item', ['name', 'mass', 'price'])


def get_mass_and_price(items: list[Item]) -> tuple[list[int], list[int]]:
    """Returns tuple of lists for mass and prices"""
    masses = [item.mass for item in items]
    prices = [item.price for item in items]
    return masses, prices


def get_memtable(items: list[Item],
                 bag_size: int
                 ) -> tuple[list[list[int], list[int]], list[int], list[int]]:
    """Generates dynamic programming memtable"""
    masses, prices = get_mass_and_price(items)
    n = len(prices)
    V = [[0 for i in range(bag_size + 1)] for j in range(n + 1)]

    for i in range(n+1):
        for a in range(bag_size + 1):
            if i == 0 or a == 0:
                V[i][a] = 0
            elif masses[i - 1] <= a:
                V[i][a] = max(prices[i - 1] + V[i - 1][a - masses[i - 1]],
                              V[i - 1][a])
            else:
                V[i][a] = V[i-1][a]
    return V, masses, prices


def get_selected_items_list(items: list[Item],
                            bag_size: int) -> list[str]:
    """Extracts items from dynamic programming memtable"""
    V, masses, prices = get_memtable(items, bag_size)
    n = len(prices)
    res = V[n][bag_size]
    a = bag_size
    items_list = []
    for i in range(n, 0, -1):
        if res <= 0:
            break
        if res == V[i-1][a]:
            continue
        else:
            items_list.append((masses[i - 1], prices[i - 1]))
            res -= prices[i-1]
            a -= masses[i-1]
    selected_stuff = []
    for search in items_list:
        for item in items:
            if (item.mass, item.price) == search:
                selected_stuff.append(item.name)
    return sorted(selected_stuff)


class TestIndexOfSum(unittest.TestCase):
    def setUp(self):
        self.items = [Item('Обручальное кольцо', 7, 49_000),
                      Item('Мобильный телефон', 200, 110_000),
                      Item('Ноутбук', 2000, 150_000),
                      Item('Ручка Паркер', 20, 37_000),
                      Item('Статуэтка Оскар', 4000, 28_000),
                      Item('Наушники', 150, 11_000),
                      Item('Гитара', 1500, 32_000),
                      Item('Золотая монета', 8, 140_000),
                      Item('Фотоаппарат', 720, 79_000),
                      Item('Лимитированные кроссовки', 300, 80_000)]

    def test_result_exist(self):
        bag_size = 500
        expected_result = [
            "Золотая монета",
            "Мобильный телефон",
            "Наушники",
            "Обручальное кольцо",
            "Ручка Паркер",
        ]
        result = get_selected_items_list(self.items, bag_size)
        self.assertEqual(expected_result, result)

    def test_result_not_exist(self):
        bag_size = 1
        expected_result = []
        result = get_selected_items_list(self.items, bag_size)
        self.assertEqual(expected_result, result)


if __name__ == "__main__":
    unittest.main()
