from _collections_abc import Iterator
from typing import Iterator, Iterable
from random import randint
import unittest


class RandomIterator(Iterator, Iterable):
    """Iterator which generates random lenght chain of random numbers
    from 1 to parameter specified on creation and protects this parameter
    from changing after creation"""
    def __init__(self, num):
        self.__num = num

    def __iter__(self):
        return self

    def __next__(self):
        rand = randint(1, self._RandomIterator__num)
        if rand == self._RandomIterator__num:
            raise StopIteration
        return rand

    @property
    def num(self):
        return self._RandomIterator__num

    @num.setter
    def num(self, new_num):
        print("""Appologies, but its not possible to change num property
                 value after creation, instantiate new object""")
        raise NotImplementedError

    @num.deleter
    def num(self):
        print("Appologies, but its not possible to delete num property")
        raise NotImplementedError


class MyList(list):
    def __repr__(self) -> str:
        output = ''
        for s in self:
            output += f"{str(s)} \\"
        return "\\\\" + output + "\\"


class TestRandomIterator(unittest.TestCase):
    def setUp(self) -> None:
        self.random_iterator = RandomIterator(10)

    def test_random_iterator_getter(self):
        self.assertEqual(10, self.random_iterator.num)

    def test_random_iterator_setter(self):
        with self.assertRaises(NotImplementedError):
            self.random_iterator.num = 10

    def test_random_iterator_deleter(self):
        with self.assertRaises(NotImplementedError):
            del self.random_iterator.num


class TestMyList(unittest.TestCase):
    def setUp(self) -> None:
        self.my_list = MyList([1, 2, 3, 4, 5])

    def test_my_list_repr(self):
        self.assertEqual("\\\\1 \\2 \\3 \\4 \\5 \\\\", str(self.my_list))


if __name__ == "__main__":
    rand_iter = RandomIterator(10)
    my_list = MyList(rand_iter)
    print(my_list)
    unittest.main()
