from typing import Iterable
import re


def extract_matrix(text: Iterable) -> list[list[int]]:
    """Gets some data in form of iterable object of strings
    and return array of integers"""
    result = []
    if len(text) == 0:
        return result
    if text[-1] == "":
        text.pop()
    if len(text) % 2 == 0:
        raise ValueError("Not valid input format. Table structure incorrect.")
    odd_line = True
    valid_border = r'^\+(-+\+)+$'  # Matches "+---+(...)+---+"
    valid_cell = r'^\|( *[0-9]+ *\|)+$'  # Matches "| number |(...)| number |"
    num_colls = len(re.findall(r'\+', text[0])) - 1
    for line in text:
        if odd_line and not re.match(valid_border, line):
            raise ValueError("Not valid input format. Border incorrect.")
        if not odd_line and not re.match(valid_cell, line):
            raise ValueError("Not valid input format. Cells incorrect.")
        if not odd_line:
            values = [int(i) for i in line[1:-1].split("|")]
            if len(values) == num_colls:
                result.append(values)
            else:
                raise ValueError("Not a Matrix")
        odd_line = not odd_line
    return result


def walk_matrix(matrix: list[list[int]]) -> list[int]:
    """Walks matrix counterclockwize and output list of values"""
    result = []
    if len(matrix) == 0:
        return result
    i = j = i_min = j_min = 0
    i_max = len(matrix) - 1
    j_max = len(matrix[0]) - 1
    # Matrix elements which we visited will be relaced with None value
    try:
        while matrix[i][j] is not None:
            # Going down
            while i <= i_max:
                result.append(matrix[i][j])
                matrix[i][j] = None
                i += 1
            j_min += 1
            i -= 1
            j += 1
            # Going right
            while j <= j_max:
                result.append(matrix[i][j])
                matrix[i][j] = None
                j += 1
            i_max -= 1
            j -= 1
            i -= 1
            # Going up
            while i >= i_min:
                result.append(matrix[i][j])
                matrix[i][j] = None
                i -= 1
            j_max -= 1
            i += 1
            j -= 1
            # Going left
            while j >= j_min:
                result.append(matrix[i][j])
                matrix[i][j] = 0
                j -= 1
            i_min += 1
            j += 1
            i += 1
    except IndexError:
        pass
    return [el for el in result if el is not None]
