from aiohttp import ClientSession, ClientError, ServerConnectionError
from asyncio import sleep
from matrixroundwalk.utils import extract_matrix, walk_matrix


async def get_matrix(url: str) -> list[int]:
    async with ClientSession() as session:
        retry = 3
        while retry > 0:
            try:
                async with session.get(url) as resp:
                    if (resp.status == 200):
                        payload = await resp.text()
                        return walk_matrix(extract_matrix(payload.split('\n')))
            except ClientError as err:
                err.add_note(f"Client error. Check input.")
                raise err
            retry -= 1
            await sleep(3 - retry)
        raise ServerConnectionError("Bad server responce")
