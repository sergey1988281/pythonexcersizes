import pytest
from matrixroundwalk.utils import extract_matrix, walk_matrix


#
# Testing extract_matrix function
#
def test_extract_matrix_void_scenario() -> None:
    input_data = []
    expected_result = []
    assert extract_matrix(input_data) == expected_result


def test_extract_matrix_base_scenario_1() -> None:
    input_data = [
        "+---+---+",
        "| 1 | 2 |",
        "+---+---+",
    ]
    expected_result = [
        [1, 2],
    ]
    assert extract_matrix(input_data) == expected_result


def test_extract_matrix_base_scenario_3() -> None:
    input_data = [
        "+---+---+---+",
        "| 1 | 2 | 3 |",
        "+---+---+---+",
        "| 4 | 5 | 6 |",
        "+---+---+---+",
        "| 7 | 8 | 9 |",
        "+---+---+---+",
    ]
    expected_result = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]
    assert extract_matrix(input_data) == expected_result


def test_extract_matrix_incorrect_border() -> None:
    input_data = [
        "+---+---+--++",
        "| 1 | 2 | 3 |",
        "+---+---+---+",
        "| 4 | 5 | 6 |",
        "+---+---+---+",
        "| 7 | 8 | 9 |",
        "+---+---+---+",
    ]
    with pytest.raises(ValueError):
        extract_matrix(input_data)


def test_extract_matrix_incorrect_cell() -> None:
    input_data = [
        "+---+---+---+",
        "| 1 | 2   3 |",
        "+---+---+---+",
        "| 4 | 5 | 6 |",
        "+---+---+---+",
        "| 7 | 8 | 9 |",
        "+---+---+---+",
    ]
    with pytest.raises(ValueError):
        extract_matrix(input_data)


def test_extract_matrix_incorrect_structure() -> None:
    input_data = [
        "+---+---+---+",
        "| 1 | 2 | 3 |",
        "+---+---+---+",
        "| 4 | 5 | 6 |",
        "| 7 | 8 | 9 |",
        "+---+---+---+",
    ]
    with pytest.raises(ValueError):
        extract_matrix(input_data)


def test_extract_matrix_not_matrix() -> None:
    input_data = [
        "+---+---+---+",
        "| 1 | 2 | 3 |",
        "+---+---+---+",
        "| 4 | 5 |",
        "+---+---+---+",
        "| 7 | 8 | 9 |",
        "+---+---+---+",
    ]
    with pytest.raises(ValueError):
        extract_matrix(input_data)


#
# Testing walk_matrix function
#
def test_walk_matrix_void_scenario() -> None:
    input_data = []
    expected_result = []
    assert walk_matrix(input_data) == expected_result


def test_walk_matrix_base_scenario_12() -> None:
    input_data = [
        "+---+---+",
        "| 1 | 2 |",
        "+---+---+",
    ]
    expected_result = [1, 2]
    assert walk_matrix(extract_matrix(input_data)) == expected_result


def test_walk_matrix_base_scenario_21() -> None:
    input_data = [
        "+---+",
        "| 1 |",
        "+---+",
        "| 2 |",
        "+---+",
    ]
    expected_result = [1, 2]
    assert walk_matrix(extract_matrix(input_data)) == expected_result


def test_walk_matrix_base_scenario_22() -> None:
    input_data = [
        "+---+---+",
        "| 1 | 4 |",
        "+---+---+",
        "| 2 | 3 |",
        "+---+---+",
    ]
    expected_result = list(range(1, 5))
    assert walk_matrix(extract_matrix(input_data)) == expected_result


def test_walk_matrix_base_scenario_33() -> None:
    input_data = [
        "+---+---+---+",
        "| 1 | 8 | 7 |",
        "+---+---+---+",
        "| 2 | 9 | 6 |",
        "+---+---+---+",
        "| 3 | 4 | 5 |",
        "+---+---+---+",
    ]
    expected_result = list(range(1, 10))
    assert walk_matrix(extract_matrix(input_data)) == expected_result


def test_walk_matrix_base_scenario_44() -> None:
    input_data = [
        "+---+---+---+---+",
        "| 1 | 12| 11| 10|",
        "+---+---+---+---+",
        "| 2 | 13| 16| 9 |",
        "+---+---+---+---+",
        "| 3 | 14| 15| 8 |",
        "+---+---+---+---+",
        "| 4 | 5 | 6 | 7 |",
        "+---+---+---+---+",
    ]
    expected_result = list(range(1, 17))
    assert walk_matrix(extract_matrix(input_data)) == expected_result
