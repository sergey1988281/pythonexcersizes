import asyncio
import aiohttp
import pytest
from aioresponses import aioresponses
from matrixroundwalk.matrixroundwalk import get_matrix


VALID_SOURCE_URL = 'https://raw.githubusercontent.com/avito-tech/python-trainee-assignment/main/matrix.txt'  # noqa
INVALID_SOURCE_URL = 'https://raw.githubusercontent.com/avito-tech/python-trainee-assignment/main/matr.txt'  # noqa
TRAVERSAL = [
    10, 50, 90, 130,
    140, 150, 160, 120,
    80, 40, 30, 20,
    60, 100, 110, 70,
]


def test_get_matrix_valid() -> None:
    assert asyncio.run(get_matrix(VALID_SOURCE_URL)) == TRAVERSAL


def test_get_matrix_wrong_url() -> None:
    with pytest.raises(aiohttp.ClientError):
        asyncio.run(get_matrix(INVALID_SOURCE_URL))


def test_mock_http_200() -> None:
    with aioresponses() as m:
        m.get(
            'http://example.com',
            status=200,
            body='+---+---+\n| 1 | 4 |\n+---+---+\n| 2 | 3 |\n+---+---+\n')
        assert asyncio.run(get_matrix("http://example.com")) == [1, 2, 3, 4]


def test_mock_http_401() -> None:
    with aioresponses() as m:
        m.get('http://example.com', status=401)
        with pytest.raises(aiohttp.ClientError):
            asyncio.run(get_matrix("http://example.com"))


def test_mock_http_404() -> None:
    with aioresponses() as m:
        m.get('http://example.com', status=404)
        with pytest.raises(aiohttp.ClientError):
            asyncio.run(get_matrix("http://example.com"))


def test_mock_http_500() -> None:
    with aioresponses() as m:
        m.get('http://example.com', status=500, repeat=True)
        with pytest.raises(aiohttp.ServerConnectionError):
            asyncio.run(get_matrix("http://example.com"))


def test_mock_http_500_retry() -> None:
    with aioresponses() as m:
        m.get('http://example.com', status=500)
        m.get('http://example.com', status=500)
        m.get(
            'http://example.com',
            status=200,
            body='+---+---+\n| 1 | 4 |\n+---+---+\n| 2 | 3 |\n+---+---+\n')
        assert asyncio.run(get_matrix("http://example.com")) == [1, 2, 3, 4]
