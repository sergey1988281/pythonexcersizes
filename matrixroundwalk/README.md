## Info
[link](https://github.com/avito-tech/python-trainee-assignment)

## Init
poetry config virtualenvs.in-project true
poetry install
poetry shell

## Validate
poetry run pytest tests -vvv

## Build
poetry build
