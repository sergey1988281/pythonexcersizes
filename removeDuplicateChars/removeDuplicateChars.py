import unittest


def removeDuplicateChars(str: str) -> str:
    """Returns a new string without duplicate chars"""
    knowSymbols = set()
    resultCollector = []
    for char in str:
        if char in knowSymbols:
            continue
        knowSymbols.add(char)
        resultCollector.append(char)
    return "".join(resultCollector)


class TestexpandNestedList(unittest.TestCase):
    def test_void(self):
        self.assertEqual(removeDuplicateChars(""), "")

    def test_general(self):
        self.assertEqual(removeDuplicateChars("abcd"), "abcd")
        self.assertEqual(removeDuplicateChars("aabbccdd"), "abcd")
        self.assertEqual(removeDuplicateChars("abcdabcd"), "abcd")


if __name__ == '__main__':
    unittest.main()
