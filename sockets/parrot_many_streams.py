import socket
from select import select


to_monitor = []

HOST = "0.0.0.0"
PORT = 2222

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((HOST, PORT))
server_socket.listen()


def accept_connection(server_socket):
    client_socket, addr = server_socket.accept()
    print("Connection from", addr)
    to_monitor.append(client_socket)


def send_message(client_socket):
    request = client_socket.recv(1024)
    if not request or request.decode("utf-8").strip() == "close":
        to_monitor.pop(to_monitor.index(client_socket))
        client_socket.close()
    else:
        client_socket.send(request)


def event_loop():
    while True:
        ready_to_read, _, _ = select(to_monitor, [], [])  # read, write, errors
        for sock in ready_to_read:
            if sock is server_socket:
                accept_connection(sock)
            else:
                send_message(sock)


if __name__ == '__main__':
    to_monitor.append(server_socket)
    event_loop()
