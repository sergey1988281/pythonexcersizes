import unittest
from typing import List, Tuple


def index_of_sum(arr: List[int], value: int) -> Tuple[int, int]:
    """Given an array and value returns indexes of elements which sum
    equals to value.
    Args:
        arr (List[Integer]): Array
        value (Integer): Value
    Returns:
        Tuple[Integer, Integer]
    Raises:
        ValueError: In case there are no elements which sum equals
    """
    complements = {}
    for i in range(len(arr)):
        complement = value - arr[i]
        if complements.get(complement, None) is None:
            complements[arr[i]] = i
        else:
            return (complements[complement], i)
    else:
        raise ValueError("Indexes not found")


class TestIndexOfSum(unittest.TestCase):
    def test_index_of_sum_exist(self):
        self.assertEqual(
            index_of_sum([4, 5, 3, 1, 2], 6),
            (1, 3))
        self.assertEqual(
            index_of_sum([4, 5, 3, 1, 2], 8),
            (1, 2))
        self.assertEqual(
            index_of_sum([4, 5, 3, 1, 2, 0], 2),
            (4, 5))

    def test_index_of_sum_absent(self):
        with self.assertRaises(ValueError):
            index_of_sum([4, 5, 3, 1, 2], 10)
        with self.assertRaises(ValueError):
            index_of_sum([4, 5, 3, 1, 2], 2)


if __name__ == "__main__":
    unittest.main()
