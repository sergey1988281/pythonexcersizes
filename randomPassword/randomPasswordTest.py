import unittest
from randomPassword import gen_passwd, misleading
from string import ascii_lowercase, ascii_uppercase, digits


class Test2Max(unittest.TestCase):
    def test_one(self):
        for _ in range(100):
            passwd = gen_passwd(10, True, True, True, True, True)
            self.assertTrue(len(passwd) == 10)
            self.assertTrue(
                    any(symbol in ascii_uppercase for symbol in passwd)
                )
            self.assertTrue(
                    any(symbol in ascii_lowercase for symbol in passwd)
                )
            self.assertTrue(any(symbol in digits for symbol in passwd))
            self.assertFalse(any(symbol in misleading for symbol in passwd))


if __name__ == '__main__':
    unittest.main()
