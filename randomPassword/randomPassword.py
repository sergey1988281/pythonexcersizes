from random import choice, shuffle
from string import ascii_lowercase, ascii_uppercase, digits, punctuation


misleading = "10IOlo"


def gen_passwd(password_len: int,
               include_digits: bool,
               include_lowercase: bool,
               include_uppercase: bool,
               include_punctuation: bool,
               exclude_misleading: bool
               ) -> str:
    """Generates strong passwords given conditions"""
    result = []
    digit_letters = set(digits)
    lowercase_letters = set(ascii_lowercase)
    uppercase_letters = set(ascii_uppercase)
    punctuation_letters = set(punctuation)
    misleading_letters = set(misleading)
    allowed_characters = []
    if exclude_misleading:
        digit_letters -= misleading_letters
        lowercase_letters -= misleading_letters
        uppercase_letters -= misleading_letters
    # To include minimum 1 digit
    if include_digits:
        result.append(choice(list(digit_letters)))
        allowed_characters.extend(digit_letters)
    # To include minimum 1 lowercase character
    if include_lowercase:
        result.append(choice(list(lowercase_letters)))
        allowed_characters.extend(lowercase_letters)
    # To include minimum 1 uppercase character
    if include_uppercase:
        result.append(choice(list(uppercase_letters)))
        allowed_characters.extend(uppercase_letters)
    # To include minimum 1 punctuation symbol
    if include_punctuation:
        result.append(choice(list(punctuation_letters)))
        allowed_characters.extend(punctuation_letters)
    # Add more characters to ensure length
    for _ in range(password_len - len(result)):
        result.append(choice(allowed_characters))
    # Shuffle before return
    shuffle(result)
    return "".join(result)


if __name__ == "__main__":
    """How many passwords to generate
       Which length
       Include numbers 0123456789?
       Include uppercase ABCDEFGHIJKLMNOPQRSTUVWXYZ?
       Include lowercase abcdefghijklmnopqrstuvwxyz?
       Include special !#$%&*+-=?@^_?
       Exclude misleading il1Lo0O?"""
    while True:
        do_continue = input(("Would you like to generate a "
                             "strong random passwords?:(Y/n)")).upper()
        if do_continue in ("N", "No"):
            print("Good bye!")
            exit()
        if do_continue not in ("Y", "YES"):
            print("Invalid option, please retry")
            continue

        password_count = input("How many?")
        password_len = input("Which length?(8 minimum)")
        if (
                not password_count.isdigit() or
                not password_len.isdigit() or
                int(password_len) < 8):
            print("Invalid option, please retry")
            continue

        include_digits = input(f"Include digits? ({digits}) (Y/n)").upper()
        include_lowercase = input(f"Include lowercase letters?"
                                  f" ({ascii_lowercase}) (Y/n)").upper()
        include_uppercase = input(f"Include uppercase letters?"
                                  f" ({ascii_uppercase}) (Y/n)").upper()
        include_punctuation = input(f"Include punctuation?"
                                    f" ({punctuation}) (Y/n)").upper()
        exclude_misleading = input(f"Exclude misleading? "
                                   f"({misleading}) (Y/n)").upper()

        if (
                include_digits not in ("Y", "YES", "N", "NO") or
                include_lowercase not in ("Y", "YES", "N", "NO") or
                include_uppercase not in ("Y", "YES", "N", "NO") or
                include_punctuation not in ("Y", "YES", "N", "NO") or
                exclude_misleading not in ("Y", "YES", "N", "NO")):
            print("Invalid option, please retry")
            continue

        for _ in range(int(password_count)):
            print(
                gen_passwd(
                    int(password_len),
                    True if include_digits in ("Y", "YES") else False,
                    True if include_lowercase in ("Y", "YES") else False,
                    True if include_uppercase in ("Y", "YES") else False,
                    True if include_punctuation in ("Y", "YES") else False,
                    True if exclude_misleading in ("Y", "YES") else False
                )
            )
